const elements = document.querySelectorAll('ul > li')

const logo = document.querySelector('.avatar')

const skillsDt = document.querySelectorAll('dl > dt')

const skillsDd = document.querySelectorAll('dl > dd')

const backgroundColorMain = document.querySelector('.body')

onClickMouse(logo, backgroundColorMain)

logo.onmouseover = logo.onmouseout = cursorMouseLogo

for (let count = 0; count < elements.length; count++) {
    elements[count].onmouseover = elements[count].onmouseout = cursorMouse
}

for (let count = 0; count < skillsDt.length; count++) {
    skillsDt[count].onmouseover = skillsDt[count].onmouseout = cursorMouseSkillsDt
}

for (let count = 0; count < skillsDd.length; count++) {
    skillsDd[count].onmouseover = skillsDd[count].onmouseout = cursorMouseSkillsDd
}

function cursorMouse(event) {

    if (event.type == 'mouseover') {
        addStyle(event.target, '1.2rem', 'blue')
    }

    if (event.type == 'mouseout') {
        setTimeout(() => {
            addStyle(event.target, '', '')
        }, 100)
    }
}

function cursorMouseLogo(event) {

    if (event.type == 'mouseover') {
        addStyle(event.target, '', '', '82%', '82%')
    }

    if (event.type == 'mouseout') {
        setTimeout(() => {
            addStyle(event.target, '', '', '', '')
        }, 100)
    }
}

function onClickMouse(nodeElements, node) {
    nodeElements.onclick = () => {
        if (node.style.backgroundColor === 'aliceblue') {
            node.style.backgroundColor = ''   
        }
        else {
            node.style.backgroundColor = 'aliceblue' 
        }
    }
}

function cursorMouseSkillsDt(event) {

    if (event.type == 'mouseover') {
        addStyle(event.target, '1.2rem')
    }

    if (event.type == 'mouseout') {
        setTimeout(() => {
            addStyle(event.target, '')
        }, 200)
    }
}

function cursorMouseSkillsDd(event) {

    if (event.type == 'mouseover') {
        addStyle(event.target, '0.9rem', 'lightgrey')
    }

    if (event.type == 'mouseout') {
        setTimeout(() => {
            addStyle(event.target, '','')
        }, 200)
    }
}

const addStyle = function (node, fontSize, color, width, height) {
    node.style.fontSize = fontSize
    node.style.color = color
    node.style.width = width
    node.style.height = height
}